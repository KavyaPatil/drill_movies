const favouritesMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/

// Solution 1

function moviesEarnedMoreThan500M(movies) {
  const entries = Object.entries(movies);

  const result = entries.filter((movie) => {
    const earning = +movie[1].totalEarnings.replace("$", "").replace("M", "");

    return earning > 500;
  });

  return Object.fromEntries(result);
}

const solution1 = moviesEarnedMoreThan500M(favouritesMovies);

console.log(solution1);



// Solution 2

function moviesGot3OscarsAnd500M(movies) {
  const entries = Object.entries(movies);

  const result = entries.filter((movie) => {
    return movie[1].oscarNominations > 3;
  });

  return Object.fromEntries(result);
}

const solution2 = moviesGot3OscarsAnd500M(solution1);

console.log(solution2);



// Solution 3

function moviesOfLeonardoDiaprio(movies) {
  const entries = Object.entries(movies);

  const result = entries.filter((movie) => {
    return movie[1].actors.includes("Leonardo Dicaprio");
  });

  return Object.fromEntries(result);
}

const solution3 = moviesOfLeonardoDiaprio(favouritesMovies);

console.log(solution3);



// Solution 4

function sortingMoviesOnIMDBRating(movies) {
  const entries = Object.entries(movies);

  const result = entries.sort((movie1, movie2) => {
    if (movie1[1].imdbRating > movie2[1].imdbRating) {
      return -1;
    } else if (movie1[1].imdbRating == movie2[1].imdbRating) {
      const movie1Earning = +movie1[1].totalEarnings
        .replace("$", "")
        .replace("M", "");
      const movie2Earning = +movie2[1].totalEarnings
        .replace("$", "")
        .replace("M", "");

      if (movie1Earning > movie2Earning) {
        return -1;
      } else {
        return 1;
      }
    } else {
      return 1;
    }
  });

  return Object.fromEntries(result);
}

const solution4 = sortingMoviesOnIMDBRating(favouritesMovies);

console.log(solution4);




// Solution 5

function groupingMoviesBasedOnGenres(movies) {
  const entries = Object.entries(movies);

  const result = {};

  entries.map((movie) => {
    console.log(movie);
    if (movie[1].genre.includes("drama")) {
      if (result.drama) {
        result.drama.push(movie);
      } else {
        result.drama = [];
        result.drama.push(movie);
      }
    } else if (movie[1].genre.includes("sci-fi")) {
      if (result.sci_fi) {
        result.sci_fi.push(movie);
      } else {
        result.sci_fi = [];
        result.sci_fi.push(movie);
      }
    } else if (movie[1].genre.includes("adventure")) {
      if (result.adventure) {
        result.adventure.push(movie);
      } else {
        result.adventure = [];
        result.adventure.push(movie);
      }
    } else if (movie[1].genre.includes("thriller")) {
      if (result.thriller) {
        result.thriller.push(movie);
      } else {
        result.thriller = [];
        result.thriller.push(movie);
      }
    } else {
      if (result.crime) {
        result.crime.push(movie);
      } else {
        result.crime = [];
        result.crime.push(movie);
      }
    }
    return;
  });

  return result;
}

const solution5 = groupingMoviesBasedOnGenres(favouritesMovies);

console.log(solution5);
